const { browserify } = require(`../index`)
const fs = require(`fs`)

const moduleName = process.argv[2]
const commandName = process.argv.length < 5 ? `file` : process.argv[3]

if(commandName == `code`){
    const code = process.argv[4]
    const result =  browserify.parse(moduleName, code)
    process.stdout.write(code)
}

if(commandName == `file`){
    const filePath = process.argv[4] || process.argv[3]
    const code = fs.readFileSync(filePath, `utf-8`)
    const result =  browserify.parse(moduleName, code)
    process.stdout.write(result)
}

if(commandName == `module`){
    const modulePath = process.argv[4]
    const modulePart = process.argv[5] || "all"
    const result =  browserify.parseModule(moduleName, modulePath, "js")
    process.stdout.write(result[modulePart])
}
