$(()=>{

  const form = $(`#password-remind-form`)
  const formInputPassword = form.find(`[name=password]`)
  const formInputPasswordConfirm = form.find(`[name=password-confirm]`)
  const errorField = $(`#error-field`)

  if(errorField.text().trim() != `...`){
      errorField.css(`visibility`, `visible`)
  }

  function errorClear(){
    errorField.css(`visibility`, `hidden`)
  }

  formInputPassword.on(`input`, errorClear)
  formInputPasswordConfirm.on(`input`, errorClear)

  form.on(`submit`, ()=>{

    if(formInputPassword.val() != formInputPasswordConfirm.val()){
      errorField.css(`visibility`, `visible`)
      errorField.text(`The password isn't the same as confirmation`)
      return false
    }

  })

})
