let scopeCounter = 0
module.exports = function(parsedCode, filePath = ``){

  const { when } = require(`@suzan_pevensive/when-js`)

  const scopeName = `___annotation_scope_${scopeCounter}`
  scopeCounter += 1

  function getDesc(nameString, type, config){

    const namePart = nameString.split(`.`)

    return {

      name: namePart.slice(-1)[0].trim(),
      scope: config.scope || namePart.slice(0, -1).join(`.`) || when(type, {
        let: () => scopeName,
        const: () => scopeName,
        else: () => `this`
      }).trim()

    }

  }

  const actions = {

    scope: (type, nameString, body, args, config) => {
      const scope = args[0]
      if(!nameString) throw `Annotations parser Errro!! \n\n In file: ${filePath}, at line: ${actualLine} \n\n` +
                      `Function scope should have at least one agrument as a name of scope`
      config.scope = scope
    },
    get: (type, nameString, body, args, config) => {
      const name = args[0]
      if(!nameString) throw `Annotations parser Errro!! \n\n In file: ${filePath}, at line: ${actualLine} \n\n` +
                      `Function get should have at least one agrument as a name og getting variable`
      const desc = getDesc(name, type, config)
      const replaceName = args[1] || desc.name
      const scope = config.scope || config.scopes[name] || scopeName
      return `${type} ${nameString}${body}`.replace(new RegExp(`${replaceName}`, `g`), `${scope}.${desc.name}`)
    },
    getter: (type, nameString, body, args, config) => {
      const objectOriented = nameString.charAt(0) == `{`
      const names = nameString.replace(/(^\{|\}$)/gm, ``).split(`,`)
      for(let index in names){
        const name = (args[index] || names[index]).trim()
        const desc = getDesc(name, type, config)
        config.scopes[name] = desc.scope
        return `Object.defineProperty(${desc.scope}, \`${desc.name}\`, {
                  get(){ return ${ body.replace(/^=/g, ``) }${objectOriented ? `.${desc.name}` : `` } },
                  set(value){ }
                })`
      }
    }

  }


  const config = { scopes: [] }
  function parseAnnotations(match, _, actionsString, _, annotationBody, endChar, index){

    config.scope = null

    const annotationBodyParts = annotationBody.split(` `)
    const annotationBodyType = annotationBodyParts[0].trim()
    const annotationBodyName = annotationBodyType == `return` ? ` ` :
                                (annotationBodyType == `class` || annotationBodyType == `function` ?
                                annotationBodyParts.slice(1).join(` `).split(`(`)[0].split(`{`)[0].trim() :
                                annotationBodyParts.slice(1).join(` `).split(`=`)[0].split(`;`)[0].trim())

    const actionNames = actionsString.split(`:`)
    for(let actionName of actionNames){
      const actionNameParts = actionName.split(`(`)
      actionName = actionNameParts[0]
      const actionBody = annotationBody.split(annotationBodyName).slice(1).join(annotationBodyName).trim()
      const actionArgs = !actionNameParts[1] ? [] : actionNameParts[1].split(`)`)[0].split(`,`).map((arg)=>arg.trim())
      const action = actions[actionName]
      if(action){
        const result = action(annotationBodyType, annotationBodyName, actionBody, actionArgs, config)
        if(result) annotationBody = result
      }
    }

    return `${annotationBody}${endChar}`

  }

  const regExpWithEnd = /(\/\/)?\s*?@browserify((:.+?)*?)\s*?\n([^]+?)((\/\/)?\s*?@browserify:end)/gm
  const regExp = /(\/\/)?\s*?@browserify((:.+?)*?)\s*?\n(.+?)(\n|$)/gm
  let newCode =  parsedCode.replace(regExpWithEnd, parseAnnotations)
  newCode =  newCode.replace(regExp, parseAnnotations)

  return `const ${scopeName} = {}; \n\n ${newCode}`

}
