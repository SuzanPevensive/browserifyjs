/**

  @description
    parse server js module to js object with codes for browser and publish as server get path

  @param {string}   app - server app
  @param {string}   libName - name of module
  @param {string}   srcDir - where find module file source
  @param {string}   ext - extensions of source files, or null for all type of files
  @param {string}   options - options for Browserify.parse
  @return {nothing}

*/
module.exports = function(app, libName, srcDir, ext, options = {}){

  const module = this.parseModule(libName, srcDir, ext, options)

  const publishOptions = options.publish || {}

  const contentType = publishOptions.contentType || `text/javascript`
  const publishName = publishOptions.name || libName.replace(/(.)([A-Z])/g, (match, before, letter)=>{
                                        return `${before}-${letter.toLowerCase()}`
                                      }).toLowerCase()
  const publishPath = (publishOptions.path || `browserify`).replace(/(^\/|\/$)/g, ``)
  const allPublishPath = `/${publishPath}/${publishName}`
  const filePublishPath = `${allPublishPath}/:name`

  app.get(allPublishPath, function(req, res){

    res.setHeader(`content-type`, contentType)
    res.send(module.all)

  })

  app.get(filePublishPath, function(req, res){

    res.setHeader(`content-type`, contentType)
    res.send(module[req.params.name] || ``)

  })

}
