const parse = require(`./parse`)
const parseModule = require(`./parse_module`)
const publishModule = require(`./publish_module`)

class Browserify{

  parse(name, jsCode, options, filePath){
    return parse.call(this, name, jsCode, options, filePath)
  }

  parseModule(libName, srcDir, ext, options){
    return parseModule.call(this, libName, srcDir, ext, options)
  }

  publishModule(app, libName, srcDir, ext, options){
    return publishModule.call(this, app, libName, srcDir, ext, options)
  }

}

module.exports = new Browserify()
