const fs = require(`fs`)
const path = require(`path`)

/**

  @description
    parse server js module to js object with codes for browser

  @param {string}   libName - name of module
  @param {string}   srcDir - where find module file source
  @param {string}   ext - extensions of source files, or null for all type of files
  @param {string}   options - options for Browserify.parse
  @return {object}

  @example "parse module"

    let object = {}
    let result = parse_module(`example`, `src`, `.js`)

    result.all == "[ code of all source files ]"
    result.someFile == "[ code of someFile.js source file ]"
    result.consts == "[ code of consts.js source file ]"

*/
module.exports = function(libName, srcDir, ext, options = {}){

  const module = { all: `` }
  if(ext && ext.charAt(0) != `.`) ext = `.${ext}`

  options.scope = options.scope || libName
  const libNameIfNotExistScript = `if(!window[\`${libName}\`]) window[\`${libName}\`] = {};`

  const files = fs.readdirSync(srcDir)
  files.forEach(file => {
    const fileName = path.basename(file, ext)
    const filePath = path.join(srcDir, file)
    if(!ext || path.extname(file) == ext){
      const code = fs.readFileSync(filePath, `utf-8`)
      const newCode = this.parse(fileName, code, options, filePath)
      module[fileName] = `${libNameIfNotExistScript} \n\n ${newCode}`
      module.all += `${ newCode } \n\n `
    }
  })

  module.all = `${libNameIfNotExistScript} \n\n ${module.all}`
  return module

}
