const annotations = require(`./annotations`)

/**

  @description
    parse server js code to browser js code

  @param {string}   name - name replaced "module.exports"
  @param {string}   jsCode - code to parse
  @param {object}   options{ - parsing options
    @param {string}   scope - scope for implementing exports
    @param {object}   require { - options for require
      @param {string}   scope - scope of parsing required local library - with path (./)
                                default: same as field options.scope
      @param {string}   globalScope - scope for global libs
                                      default: window
      @param {function}   parser - parser for required libraries replacement
        @argument {string}    name - name of parsing required library
        @argument {string}    scope - value of options.require.scope
        @argument {string}    globalScope - value of options.require.globalScope
        @argument {function}  defaultParser - defaultParser method with binded arguments
    }
  }
  @param {string}    filePath - filename for error parser
  @return {string}

  @example "content of file": "example.js"

    const { lib } = require(`someLib`)

    module.exports = {

      let value = lib(12)

    }

  @example "parse file": "example.js"

    let object = {}
    parse(`example`, "[ code of example.js ]", {
      scope: `object`
      require: {
        parser: (name) => {
          return name == `someLib` ? `{ lib: ()=>{ ... } }` : ``
        }
      }
    })

  @example "result of parsing file": "example.js"

    const { lib } = { lib: ()=>{ ... } }

    object.example = {

      let value = lib(12)

    }

*/
module.exports = function(name, jsCode, options = {}, filePath = ``){

  const { when } = require(`@suzan_pevensive/when-js`)

  function getScope(scope){
    return when(scope, {
      "undefined": { key: () => typeof scope == `undefined`, result: ()=>`window.` },
      "null": { key: null, result: ()=>`` },
      "": ()=>``,
      "let": ()=>`let `,
      "var": ()=>`var `,
      "const": ()=>`const `,
      else: ()=>`${scope}.`,
    })
  }

  function defaultParser(name, scope, globalScope){
    const reqNameParts = name.split(`/`)
    const reqName = reqNameParts.slice(-1)[0]
    if(name.charAt(0) == `.`) return `${scope}[\`${reqName}\`]`
    else{
      const reqNameFirstLetterUpperCase = reqName.charAt(0).toUpperCase()
      const reqNameDashToUpperCase = reqName.slice(1).replace(/-(\w)/g, (match, letter)=>{
                                        return letter.toUpperCase()
                                     })
      const reqLibName = `${reqNameFirstLetterUpperCase}${reqNameDashToUpperCase}`
      return `${globalScope}[\`${reqLibName}\`]`
    }
  }

  let newCode = jsCode
  newCode = newCode.replace(`module.exports`, `${getScope(options.scope)}${name}`)

  const requireOptions = options.require || {}
  const parser = requireOptions.parser || defaultParser
  newCode = newCode.replace(/require\(([^]+?)\)/g, (match, name, offset, string)=>{
    const requireScope = getScope(requireOptions.scope || options.scope).replace(/\.$/g, ``)
    const requireGlobalScope = getScope(requireOptions.globalScope).replace(/\.$/g, ``)
    const defaultParserBinded = (parsedName)=>defaultParser(parsedName, requireScope, requireGlobalScope)
    return parser(name.replace(/[`'"]+/g, ``), requireScope, requireGlobalScope, defaultParserBinded)
  })

  return `(new (function(){ \n\n ${annotations(newCode, filePath)} \n\n }));`

}
